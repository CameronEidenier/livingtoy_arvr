﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public Transform startPoint;
	public Transform endPoint;

	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp(startPoint.position, endPoint.position, Mathf.PingPong(Time.time * 0.25f, 1.0f));
		transform.Rotate(Vector3.forward, 90 * Time.deltaTime);
	}
}
