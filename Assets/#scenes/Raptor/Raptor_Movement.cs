﻿using UnityEngine;
using System.Collections;
//using DG.Tweening;

public class Raptor_Movement : MonoBehaviour {
    
    // Use this for initialization
    public int pickpath;
    string pathString;
	Vector3 previous = new Vector3();

    void Start () {
        if (pickpath == 1) pathString = "RapterPath1";
        if (pickpath == 2) pathString = "RapterPath2";
        if (pickpath == 3) pathString = "RapterPath3";
        if (pickpath == 4) pathString = "RapterPath4";
        CreateSequence();
    }
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 vel = (transform.position - previous) / Time.deltaTime;

		previous = transform.position;

		if (vel != new Vector3(0,0,0))
			transform.rotation = Quaternion.LookRotation(vel);


	}
    void CreateSequence()
    {
        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath(pathString), "time", 50f, "easeType", iTween.EaseType.linear, "orienttopath", true, "oncomplete", "CreateSequence"));


    }
}
