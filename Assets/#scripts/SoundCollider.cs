﻿using UnityEngine;
using System.Collections;

public class SoundCollider : MonoBehaviour {

	SphereCollider col;
	AudioSource player;
	// Use this for initialization
	void Start () 
	{	
		col = GetComponent<SphereCollider>();
		player = GetComponent<AudioSource>();
	}
	
	void OnTriggerEnter(Collider other)
	{
		Debug.Log("Trigger");
		player.Play();
	}
}
