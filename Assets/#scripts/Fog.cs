﻿using UnityEngine;
using System.Collections;

public class Fog : MonoBehaviour
{

	public bool fogEnabled;
	public Color fogColor;
	public float fogDensity;
	public float fogStartDistance;
	public float fogEndDistance;
	public FogMode fogMode;
	// Use this for initialization
	void Start () {
		BuildFogSettings();

	}

	public void BuildFogSettings()
	{
		RenderSettings.fog = fogEnabled;

		RenderSettings.fogColor = fogColor;
		RenderSettings.fogDensity = fogDensity;
		RenderSettings.fogStartDistance = fogStartDistance;
		RenderSettings.fogEndDistance = fogEndDistance;
		RenderSettings.fogMode = fogMode;

	}
	// Update is called once per frame
	void Update () {
	
	}
}
