﻿using UnityEngine;
using System.Collections;

public class StegoController : MonoBehaviour 
{

	Vector3 target;
	Vector3 desiredVel;
	Vector3 steering = new Vector3();
	Vector3 velocity = new Vector3();
	public float speed = 1;
	public float maxForce = 1;
	public float maxVelocity = 2;
	public float maxSeeAhead = 3;
	public float maxAvoidanceForce = 5;


		// Use this for initialization
	void Start () {
		target = new Vector3(-200, transform.position.y, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () 
	{

		float step = speed * Time.deltaTime;


		if (transform.position.x < -100 )
		{
			Destroy(gameObject);
			return;
		}
		//transform.position = Vector3.MoveTowards(transform.position, destination, step);

		SeekTarget();
		PositionSelfOnSurface ();


	}

	public void SeekTarget()
	{
		desiredVel = target - transform.position;

		//Debug.Log(desiredVel);
		float dist = desiredVel.magnitude;


		desiredVel.Normalize ();


	

		steering = desiredVel - velocity;
		//steering = steering + Separation ();
		steering = steering + CollisionAvoidance ();


		steering = Vector3.ClampMagnitude (steering, maxForce);
		steering = steering / 1;


		float angle = Vector3.Angle (steering, velocity);
		//Debug.Log(angle);


		velocity = Vector3.Lerp(velocity, Vector3.ClampMagnitude (velocity + steering , maxVelocity ), .1f );
		

		velocity.y = 0;

		transform.position = transform.position + velocity;



		//lerp
		//velocity = Vector3.ClampMagnitude (velocity + steering , maxVelocity);

	

		if (velocity.magnitude > 0) //prevent them from rotating in place when not moving
		{
			Quaternion rotation = Quaternion.LookRotation(velocity);
			transform.rotation = Quaternion.Lerp(transform.rotation, rotation, .1f);
		

		}

	}

	void PositionSelfOnSurface()
	{
		RaycastHit hit;
		if (Physics.Raycast(transform.position + new Vector3(0, 10, 0), Vector3.down, out hit, 1000f, 1 << LayerMask.NameToLayer("Terrain")))
		{
			transform.Translate(0, hit.point.y - transform.position.y, 0);
			transform.rotation = Quaternion.LookRotation(Vector3.Exclude(hit.normal, transform.forward), hit.normal);

		}

	}


	Vector3 CollisionAvoidance()
	{
		Vector3 avoidanceForce = new Vector3();
		Vector3 normVelocity = velocity;
		normVelocity.Normalize();

		Vector3 ahead = transform.position + normVelocity * maxSeeAhead;
		RaycastHit hit;
		Ray ray = new Ray(transform.position, transform.forward);

		if (Physics.Raycast(ray, out hit, maxSeeAhead, 1 << LayerMask.NameToLayer("Player")))
		{
			avoidanceForce.x = ahead.x - hit.transform.position.x;
			avoidanceForce.z = ahead.z - hit.transform.position.z;

			avoidanceForce.Normalize();
			avoidanceForce = avoidanceForce * maxAvoidanceForce;

		}
		else
		{
			ahead = ahead * 0;
		}

		return avoidanceForce;
	}



}
