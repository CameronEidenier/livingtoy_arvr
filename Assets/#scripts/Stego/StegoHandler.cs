﻿using UnityEngine;
using System.Collections;

public class StegoHandler : MonoBehaviour {


	public float spawnTime = 2; //every 2 seconds;


	float timeElapsed = 0;

	
	// Update is called once per frame
	void Update () 
	{
		timeElapsed += Time.deltaTime;

		if (timeElapsed >= spawnTime)
		{
			timeElapsed -= spawnTime;
			GameObject go = Instantiate(Resources.Load("Stego"), new Vector3(), new Quaternion()) as GameObject;
			go.transform.parent = this.transform;

			go.transform.localPosition = new Vector3(0, 0, Random.Range(-20, 20));



		}
	}
}
