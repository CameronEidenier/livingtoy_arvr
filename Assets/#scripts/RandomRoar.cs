﻿using UnityEngine;
using System.Collections;

public class RandomRoar : MonoBehaviour {

	Animator anim;
	AudioSource audioPlayer;

	float timer = 0;
	float timeToHit = 10;
	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
		audioPlayer = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (GetComponentInChildren<SkinnedMeshRenderer>().enabled == false)
			return;

		timer += Time.deltaTime;

		if (timer > timeToHit)
		{
			timer = 0;
			anim.SetBool("roar", true); //roar should have an exit time that leads back into idle.
			Invoke("SetBoolFalse", 1f);
			Invoke("Roar", 2f);
		}
	}


	void SetBoolFalse()
	{
		anim.SetBool("roar", false);
	}


	void Roar()
	{
		audioPlayer.Play();
	}
}
